package aoc3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Day10Joltage {
		static final Logger logger = LogManager.getLogger("Joltages");
		public class Traversal {
			int _fromIndex;
			int _toIndex;
		}
		
		protected Set<Traversal> _possibleTraversals;
		
		protected List<Integer> _availableDevices;
		
		
		protected ArrayList<Day10Joltage> _pluggedIntoMe;
		protected Day10Joltage _whatAmIPluggedInto;
		int _joltage;
		public ArrayList<Integer> _remainingAdapters;
		protected boolean _viableTree = true;
		
		public Day10Joltage(int joltage) {
			_joltage  = joltage;
			_pluggedIntoMe = new ArrayList<>();
			_remainingAdapters = new ArrayList<>();
			_availableDevices = new ArrayList<>();
			_possibleTraversals = new HashSet<>();
			
			
			
		}
		
		protected Day10Joltage processRemaining() {
			logger.debug("Processing Remaining on "+_joltage+" it has "+_remainingAdapters.size()+" to process");
			int remain = 0;
			
			int i =0;
			
			while(_viableTree &&  i<_remainingAdapters.size()) {
				int adapterJoltage = _remainingAdapters.get(i);
				if (adapterJoltage < _joltage) {
					logger.debug("Going to remove "+adapterJoltage+" from "+_joltage+"becuase it is too small");
					_remainingAdapters.remove(_remainingAdapters.indexOf(adapterJoltage));
				    _viableTree = false;
				} else {
					
					if (canAcceptJoltage(adapterJoltage)) {
						Day10Joltage child = new Day10Joltage(adapterJoltage);

						child.plugInto(this);
						ArrayList<Integer> passOn = new ArrayList();
						if (_remainingAdapters.size() > 0) {
							passOn.addAll(_remainingAdapters);
							passOn.remove(passOn.indexOf(adapterJoltage));	


							child._remainingAdapters =passOn;
							if (child._remainingAdapters.size() == 0 && child._viableTree) {
								Day10Joltage device = new Day10Joltage(child._joltage+3);
								device.plugInto(child);
								logger.info("We found it "+getChainToSeat());
								logger.info("Answer is "+getQuestionAnswer());
								return device;
							}
							child.processRemaining();
						} 
					} 
				}
				i++;
				
			}
			return null;
		}
		protected boolean canAcceptJoltage(int proposedJoltage) {
			// _joltage is me
			// proposedJoltage  - We want to know if that can be plugged in to me
			return proposedJoltage >= _joltage && proposedJoltage <= _joltage+3;
		}
		
		protected String getChainToSeat() {
			String list = "";
			Day10Joltage chain = this;
			while (chain._whatAmIPluggedInto != null ) {
				list += chain._joltage;
				list += ":";
				
				chain = chain._whatAmIPluggedInto;		
			}
			return list;
			
		}
		
		protected int getQuestionAnswer() {
			int ret =0 ;
			// We want <NumberOfOneJoltDiffs * NumberOfThreeJoltDiffs
			
			int numOneJoltDiffs = 0;
			int numThreeJoltDiffs = 0;
			int diff;
			Day10Joltage chain = this._pluggedIntoMe.get(0);


			while (chain._whatAmIPluggedInto != null ) {
				diff = chain._joltage - chain._whatAmIPluggedInto._joltage;
				logger.debug("Joltage is "+chain._joltage);

				if (diff == 1) {
					numOneJoltDiffs++;
					logger.debug("One Jolt Diffs "+numOneJoltDiffs);
				} 
				if (diff == 3) {
					numThreeJoltDiffs++;
					logger.debug("Three Jolt Diffs "+numThreeJoltDiffs);

				}
				chain = chain._whatAmIPluggedInto;	
			}
			numThreeJoltDiffs++; // Because of my device
			ret = numOneJoltDiffs*numThreeJoltDiffs;
			
			return ret;
			
		}

		protected boolean canBePluggedInto(Day10Joltage possibleParent) {
			 return possibleParent.canAcceptJoltage(_joltage);
		}
		
		protected void populateTraversals() {
			logger.debug("Populating Traversals ");
			String setStr="";
			for (Integer dev : _availableDevices) {
				setStr += dev+" , ";
			}
			logger.debug("Set is "+setStr);
			
			
		}

		public void plugInto(Day10Joltage parent) {
			_whatAmIPluggedInto = parent;
			parent._pluggedIntoMe.add(this);
		}

		public void addAvailableAdapterValues(List<Integer> joltages) {
			_availableDevices.addAll(joltages);
			
		}

		public ArrayList<Integer> getBiggestValidListIncludingThisAndDevice() {
			Collections.sort(_availableDevices);
			ArrayList<Integer> plugSeq = new ArrayList<Integer>();
			plugSeq.add(_joltage);
			// Basically go through them in order.  If ever the 'next' one is more than three away
			// then we can't use that
			
			int currentVal = _joltage;
			int pos=0;
			boolean ok = true;
			while (ok && pos < _availableDevices.size()) {
				if (_availableDevices.get(pos) > currentVal+3 ) {
					ok = false;
				} else {

					currentVal=_availableDevices.get(pos);
					plugSeq.add(currentVal);
					pos++;
				}
			}
			plugSeq.add(currentVal+3);
			return plugSeq;
		}
		
		
		public static int getNumberOfDiffs(ArrayList<Integer> differences, int difference) {
			int count =0;
			for (int position = 0; position < differences.size()-1; position++) {
				if (differences.get(position+1) - differences.get(position) == difference) {
					count++;
				}
			}
			return count;
		}

		public static int countPaths(List<Integer> joltages, int i, int j) {
			int indexOfi = joltages.indexOf(i);
			int indexOfj = joltages.lastIndexOf(j);
			if (j-i>3) {
				return -1;
			}
			
			if (indexOfj-indexOfi == 1) {
				return 1;
			}
			
			
			
			return 0;
		}
		

}
