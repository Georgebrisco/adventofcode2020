package aoc3;

import java.awt.color.ICC_ColorSpace;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BagRuleProcessor {
	public BagRuleProcessor() {
		super();
		_allBags = new Hashtable<>() ;
	}

	static final Logger logger = LogManager.getLogger("BagRuleProcessor");
	protected Hashtable<String, BagDetails> _allBags;
	
	protected BagDetails getBag(String col) {
		return _allBags.get(col);
	}
	

	private BagDetails getBagDetails(String colour) {
		BagDetails dets = null;
		if (_allBags.containsKey(colour)) {
			dets = _allBags.get(colour);
			
		} else {
			dets = new BagDetails(colour);
		}
		return dets;
	}
	

	public void processRuleSet(String[] ruleSet) {
		logger.debug("Processing Ruleset");
		for (String rule : ruleSet) {
			processRule(rule);
		}
		for (String col : _allBags.keySet()) {
			BagDetails bd = _allBags.get(col);
			logger.debug(bd);
			
		}
		
	}

	public void processRule(String rule) {
		// light red bags contain 1 bright white bag, 2 muted yellow bags.
		logger.debug("Processing :"+rule);

		String containingColour = rule.substring(0, rule.indexOf(" bags contain")).trim();
		logger.debug("Containing Colour :"+containingColour);
		
		BagDetails containingBag = getBagDetails(containingColour);
		_allBags.put(containingColour, containingBag);
		
		if (rule.contains("no other bags")) {
			logger.debug("No other Bags");
		} else {
			String[] containedParts = rule.substring(rule.indexOf(" bags contain")+" bags contain".length()).split(",");
			for (String contained : containedParts) {
				contained=contained.trim();
				
				String numberPart = contained.substring(0, contained.indexOf(" ")).trim();
				int count = Integer.parseInt(numberPart.trim());
				String colour = contained.substring(contained.indexOf(" "), contained.indexOf(" bag")).trim();
				logger.debug("<"+count+"> of <"+colour+">");
				BagDetails containedBag = getBagDetails(colour);
				_allBags.put(colour, containedBag);
				
				containedBag.addContainer(containingBag);
				containingBag.addContained(containedBag, count);
			}
		}
	}
}
