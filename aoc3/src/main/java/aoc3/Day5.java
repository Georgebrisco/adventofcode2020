package aoc3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Day5 {
	static final Logger logger = LogManager.getLogger("Day3");

	public static void main(String[] args) {
		logger.info("Starting Day5");
		int maxid = 0;
		int minid = Integer.MAX_VALUE;
		int runningtotal = 0;
		

		try {
			File f = new File("D:\\temp\\boardingpasses.txt");
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			while ((readLine = b.readLine()) != null) {
				if (readLine.length() > 0) {
					SeatLocationDecoder decoder = new SeatLocationDecoder(readLine);
					maxid = decoder._seatID > maxid ? decoder._seatID : maxid;
					minid = decoder._seatID < minid ? decoder._seatID : minid;
					runningtotal += decoder._seatID;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Min SeatId was "+minid+ " Max Seat ID was " + maxid+" RunningTotal was "+runningtotal+" Sum was "+SeatLocationDecoder.getSumBetween(minid-1, maxid)+" Difference "+(SeatLocationDecoder.getSumBetween(minid-1, maxid)-runningtotal));
	}

}
