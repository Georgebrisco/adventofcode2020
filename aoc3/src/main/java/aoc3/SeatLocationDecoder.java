package aoc3;

public class SeatLocationDecoder {
	public SeatLocationDecoder(String passString) {
		super();
		this._nativeSting = passString;
		
		_rowString = _nativeSting.substring(0, _nativeSting.length()-3);
		_seatString = _nativeSting.substring( _nativeSting.length()-3);
		
		_row = 0;
		_seat = 0;
		
		_rowString.chars().forEachOrdered(posChar -> _row = posChar=='F' ? _row << 1 : (_row+1) << 1 );  
		_row = _row >> 1;
		
		_seatString.chars().forEachOrdered(posChar -> _seat = posChar=='L' ? _seat << 1 : (_seat+1) << 1 );  
		_seat = _seat >> 1;
		
		_seatID = _row*8+_seat;
	}
	
	
	protected String _nativeSting;
	protected String _rowString;
	protected String _seatString;
	
	protected int _row;
	protected int _seat;
	protected int _seatID;
	
	public static int getSumBetween(int i, int j) {
		return getSumTo(j)-getSumTo(i);
	}
	
	public static int getSumTo(int n) {
		System.out.println("Sum to "+n+ " returning "+(n * (n + 1)) / 2);
		return (n * (n + 1)) / 2; 
	}

}
