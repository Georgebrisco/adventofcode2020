package aoc3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashSet;

public class Day7 {

	public static void main(String[] args) {
		BagRuleProcessor processor = new BagRuleProcessor();
		

		try {
			File f = new File("D:\\temp\\bagrules.txt");
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			while ((readLine = b.readLine()) != null) {
				if (readLine.length() > 0) {
					processor.processRule(readLine);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		BagDetails shinygold = processor.getBag("shiny gold" );
		LinkedHashSet<BagDetails> pars = shinygold.getAllParents();
		System.out.println("Shiny Gold can be contained in "+pars.size()+" bags");
		System.out.println("Shiny Gold Contains "+shinygold.enclosedCount()+" bags");

	}

}
