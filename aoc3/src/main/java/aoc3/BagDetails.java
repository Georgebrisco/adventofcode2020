package aoc3;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BagDetails {
	static final Logger logger = LogManager.getLogger("BagRuleProcessor");
	

	protected String _colour;
	protected ArrayList<BagDetails> _containingBags;
	protected Hashtable<BagDetails, Integer> _containedBagCount;
	
	public BagDetails(String colour) {
		logger.debug("Creating BagDetails :"+colour);
		_colour = colour;
		_containingBags = new ArrayList<>();
		_containedBagCount = new Hashtable<>();
	}

	public String getColour() {
		
		return _colour;
	}

	public int getNumberOfContainedColours() {
		return _containedBagCount.size();
	}

	public int getNumberOfPossibleImmediateContainers() {
		return _containingBags.size();
	}

	public void addContainer(BagDetails containingBag) {
		logger.info("Adding a parent of "+containingBag._colour+" to "+_colour);
		
		_containingBags.add(containingBag);
		
	}

	public void addContained(BagDetails containedBag, int count) {
		logger.info("Adding a child of "+containedBag._colour+" to "+_colour);
		_containedBagCount.put(containedBag, count);
		
	}

	public LinkedHashSet<BagDetails> getAllParents() {

		logger.debug("Getting Parents of "+_colour);
		LinkedHashSet<BagDetails> parents = new LinkedHashSet<BagDetails>();
		parents.addAll(_containingBags);
		for (BagDetails parentBag : _containingBags) {
			parents.addAll(parentBag.getAllParents());
		}
		
		return parents;
	}

	public int enclosedCount() {
		logger.debug("Getting enclosed count of "+_colour);
		
		// First the directly enclosed bags
		int count = 0;
		for (BagDetails encBag : _containedBagCount.keySet()) {
			count += _containedBagCount.get(encBag);
		}
		// And now we need to add the indirectly enclosed bags.
		for (BagDetails encBag : _containedBagCount.keySet()) {
			count += _containedBagCount.get(encBag)*encBag.enclosedCount();
		}
		
		logger.debug("Returning "+count+" bags for "+_colour);
		return count;
	}
}
