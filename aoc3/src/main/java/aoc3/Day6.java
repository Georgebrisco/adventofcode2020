/**
 * 
 */
package aoc3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author George
 *
 */
public class Day6 {

	/**
	 * @param args
	 */
	static final Logger logger = LogManager.getLogger("Day3");

	public static void main(String[] args) {
		int  maxScore = 0;
		int total = 0;
		int groupScore=0;

		logger.info("Starting Day4");
		try {
			File f = new File("D:\\temp\\customs.txt");
			BufferedReader b = new BufferedReader(new FileReader(f));
			String answers = "";

			String readLine = "";
			while ((readLine = b.readLine()) != null) {
				if (readLine.length() == 0) {
					logger.debug("New Line. Next id");
					Day6AnswerSet as = new Day6AnswerSet(answers);
					String common = as.getCommonAnswers();
					System.out.println("Common is <"+common+"> from <"+answers+">");
					groupScore += common.length();
					
					System.out.println("Group Score is "+getStringWithUniqueElements(answers).length());
					total += getStringWithUniqueElements(answers).length();
					maxScore = getStringWithUniqueElements(answers).length() > maxScore ? getStringWithUniqueElements(answers).length() : maxScore;
					
					

					answers ="";
					
					
					
				} else {
					answers += " " + readLine;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("MaxScore is "+maxScore+" Total is "+total+" Group is "+groupScore);
	}
	
	static protected String getStringWithUniqueElements( String inp) {
		System.out.println("<"+inp+">");
		StringBuilder retChars = new StringBuilder();
		
		for (char c='a'; c<='z'; c++) {
			if (inp.indexOf(c) >=0 ) {
				retChars.append(c);
			}
		}
		
		return retChars.toString();
		
		
	}

}
