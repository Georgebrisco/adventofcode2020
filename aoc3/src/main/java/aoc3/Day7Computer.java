package aoc3;

import java.awt.color.ICC_ColorSpace;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Day7Computer {
	static final Logger logger = LogManager.getLogger("Day7Computer");
	
	public Day7Computer() {
		super();
		_memory = new ArrayList<String>();
		_savedState = new ArrayList<String>();
		_loopDetection = new ArrayList<>();
		_instructionPointer = 0;
		_accumulator =0;
	}

	protected ArrayList<String> _memory;
	protected ArrayList<String> _savedState;

	protected int _instructionPointer;
	protected int _accumulator;
	protected ArrayList<Integer> _loopDetection;
	protected int _instructionsExecuted;
	
	protected boolean isEndState() {
		if (_instructionsExecuted >= 999999) {
			logger.warn("ABORTING : FAILSAFE");
		}
		return _loopDetection.contains(_instructionPointer) || _instructionsExecuted >= 999999 || _instructionPointer == _memory.size();
	}
	
	protected void loadLine(String instructionLine) {
		String[] items = instructionLine.split(" ");
		for (String opItem : items) {
			_memory.add(opItem);
		} 
	}
	
	protected int getNopOrJmpAfter(int lookFrom) {
		while (lookFrom < _memory.size() && !_memory.get(lookFrom).equals("nop") && !_memory.get(lookFrom).equals("jmp") ) {
			lookFrom++;
		}
		return lookFrom;
	}

	public static void main(String[] args) {
		Day7Computer processor = new Day7Computer();
		
		logger.info("Starting Day 8 Part 1");
		try {
			File f = new File("D:\\temp\\Day7Program1.txt");
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			while ((readLine = b.readLine()) != null) {
				if (readLine.length() > 0) {
					processor.loadLine(readLine);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		processor.runProgram();

		logger.info("Starting Day 8 Part 2");
		processor.reset();
		
		int pos = 0;
		int returnCode = 1;
		processor.saveState();
		String op = "";
		String changed = "";
		
		while (returnCode > 0) {
			processor.restoreState();
			processor.reset();
			
			pos = processor.getNopOrJmpAfter(pos);
			op = processor._memory.get(pos);
			changed = op.equals("nop") ? "jmp" : "nop";
			processor._memory.set(pos, changed);
			returnCode = processor.runProgram();
			pos++;
		}
		
		
		
		
		

	}

	protected void reset() {
		_instructionPointer = 0;
		_accumulator = 0;
		_loopDetection.clear();
		_instructionsExecuted=0;
	}
	
	protected void saveState() {
		_savedState.clear();
		_savedState.addAll(_memory);
	}

	protected void restoreState() {
		_memory.clear();
		_memory.addAll(_savedState);
	}

	

	public void loadProgram(String[] program) {
		for (String line : program) {
			loadLine(line.trim());
		}
	}
	
	public int runProgram() {
		while (!isEndState()) {
			_instructionsExecuted++;
			String opCode = _memory.get(_instructionPointer);
			_loopDetection.add(_instructionPointer);
			Integer operand = Integer.parseInt(_memory.get(_instructionPointer+1));
			logger.info("Processing "+opCode+" "+operand);
			switch (opCode) {
			case "nop":
				_instructionPointer+=2;
				break;
			case "acc":
				// Just amend the accumulator.  IP will be OK
				_accumulator = _accumulator + operand;
				_instructionPointer+=2;
				break;
			case "jmp":
				_instructionPointer += 2*operand;
				break;
			default:
				break;
			}
		}
		

		if (_instructionPointer == _memory.size()) {
			logger.info("Program Completed.  Acc <"+_accumulator+">" );
			return 0;
		} else {
			logger.info("Program Aborted.  Acc <"+_accumulator+">" );
			return 1;
		}
		
	}

}
