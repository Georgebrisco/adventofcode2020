package aoc3;

import java.util.ArrayList;

public class Day6AnswerSet {
	protected String _rawInput;
	protected String[] _individualAnswers;
	public Day6AnswerSet(String _rawInput) {
		super();
		System.out.println("Constructing from <"+_rawInput+">");
		this._rawInput = _rawInput;
		_individualAnswers=_rawInput.trim().split(" ");
		
	}
	
	protected String getOneOfShortestStrings() {
		int shortestLen = Integer.MAX_VALUE;
		int indexOfShortest = -1;
		for (int strIndex = 0; strIndex < _individualAnswers.length; strIndex ++) {
			String answer = _individualAnswers[strIndex];
			if (answer.length() < shortestLen) {
				shortestLen = answer.length();
				indexOfShortest = strIndex;
			}
		}
		return _individualAnswers[indexOfShortest];
		
	}

	public String getCommonAnswers() {
		String shortAnswer = getOneOfShortestStrings();
		StringBuilder commons = new StringBuilder();
		

		for (int i=0; i<shortAnswer.length(); i++) {
			String a = shortAnswer.substring(i, i+1);
			if (existsInAllAnswers(a)) {
				commons.append(a);
			}
		}
		
		return commons.toString();
	}

	private boolean existsInAllAnswers(String ans) {
		boolean existsEverywhere = true;
		for (String answer : _individualAnswers) {
			existsEverywhere = !answer.contains(ans) ? false : existsEverywhere;
		}
		return existsEverywhere;
	}
	
	
	

}
