package aoc3;

import java.util.Hashtable;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Floorplan {
	static final Logger logger = LogManager.getLogger("Floorplan");

	public Floorplan(int i) {
		_width = i;
		_seats = new Hashtable<>();
	}

	protected int _width;
	protected int _height = 0;

	public enum Direction {
		N, NE, E, SE, S, SW, W, NW
	};

	protected Hashtable<Integer, Boolean> _seats;

	public void addRow(String row) {
		logger.debug("Adding " + row);
		if (row.length() != _width) {
			logger.error("Trying to add Odd Row Lngth was "+row.length());
		} else {
			for (int i = 0; i < row.length(); i++) {
				if (row.substring(i, i + 1).equals("#")) {

					_seats.put(((_height * _width) + i), true);
				} else if (row.substring(i, i + 1).equals("L")) {

					_seats.put(((_height * _width) + i), false);
				}
			}
			_height++;
		}
	}

	public int getLoc(int i, int j) {
		return i + _width * j;
	}

	public int getKey(int loc, Direction dir) {
		int ret = -1;
		if ((dir == Direction.NE || dir == Direction.E || dir == Direction.SE) && (loc % _width == _width-1)) {
			return ret;
		}
		if ((dir == Direction.NW || dir == Direction.W || dir == Direction.SW) && (loc % _width == 0)) {
			return ret;
		}

		if (dir == Direction.N) {
			ret = (loc - _width);
		} else if (dir == Direction.NE) {
			ret = (loc - _width) + 1;
		} else if (dir == Direction.E) {
			ret = loc + 1;
		} else if (dir == Direction.SE) {
			ret = loc + _width + 1;
		} else if (dir == Direction.S) {
			ret = loc + _width;
		} else if (dir == Direction.SW) {
			ret = (loc + _width) - 1;
		} else if (dir == Direction.W) {
			ret = loc -1;
		} else if (dir == Direction.NW) {
			ret = (loc - _width) - 1;
		}

		if (ret < 0 || ret >= _width * _height) {
			ret = -1;
		}
		return ret;
	}
	
	public int getAdjacentOccupiedSeats(int i) {
		int occupied = 0;
		if (isPerson(getKey(i, Direction.N))) {
			occupied++;
		}
		if (isPerson(getKey(i, Direction.NE))) {
			occupied++;
		}
		if (isPerson(getKey(i, Direction.E))) {
			occupied++;
		}
		if (isPerson(getKey(i, Direction.SE))) {
			occupied++;
		}
		if (isPerson(getKey(i, Direction.S))) {
			occupied++;
		}
		if (isPerson(getKey(i, Direction.SW))) {
			occupied++;
		}
		if (isPerson(getKey(i, Direction.W))) {
			occupied++;
		}
		if (isPerson(getKey(i, Direction.NW))) {
			occupied++;
		}

		
		
		
		
		
		return occupied;
	}


	public String toStraightString() {
		return toString(false);
	}
	
	
	public String toString() {
		return toString(true);
	}
	
	public String toString(boolean withCarriageReturns) {
		String ret = "";

		for (int row = 0; row < _height; row++) {
			for (int col = 0; col < _width; col++) {

				int loc = getLoc(col, row);

				if (!isFloor(loc)) {
					if (isPerson(loc)) {
						ret += "#";
					} else {
						ret += "L";
					}
				} else {
					ret += ".";
				}
			}
			if (withCarriageReturns) {
				ret += "\n";
			}
		}

		return ret;
	}

	private boolean isPerson(int loc) {
		if (_seats.containsKey(loc)) {
			return _seats.get(loc);
		} else {
			return false;
		}
	}

	private boolean isFloor(int loc) {
		return !(_seats.containsKey(loc));
	}

	public void load(String floorplan) {

		logger.debug("Loading "+floorplan);
		_seats.clear();
		for (int i=0; i<floorplan.length(); i++) {

			if (floorplan.substring(i, i+1).equals("L")) {

				_seats.put(i, false);
			} else if (floorplan.substring(i, i+1).equals("#")) {

				_seats.put(i, true);
			} else {

			}
		}
		_height=floorplan.length()/_width;
		
	}

	public String iterate() {
		String ret = "";
		for (int row = 0; row < _height; row++) {
			for (int col = 0; col < _width; col++) {
				int loc = getLoc(col, row);
				if (loc==9) {
					logger.debug("so...");
				}
				if (isFloor(loc) == false) {
					if (isPerson(loc) ) {
						if ( getAdjacentOccupiedSeats(loc) >= 4) {
							ret+="L";
						} else {
							ret += "#";
						}
					} else if (!isPerson(loc) && getAdjacentOccupiedSeats(loc) ==  0) {
						ret += "#";
					} else {
						ret+= getRepresentation(loc);
					}
				} else {
					ret += ".";
				}
			}

		}

		return ret;
	}

	private String getRepresentation(int loc) {
		if (isFloor(loc)) {
			return ".";
		} else if (isPerson(loc) ) {
			return "#";
		} else {
			return "L";
		}
	}
	
	protected int getOccupied() {
		int occupied = 0;
		for (int key : _seats.keySet()) {
			if (_seats.get(key) == true) {
				occupied++;
			}
		}
		
		return occupied;	
	}


}
