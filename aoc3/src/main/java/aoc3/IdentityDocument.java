package aoc3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IdentityDocument {
	static final Logger logger = LogManager.getLogger("IdentityDoc");
	//According to Spec, a Passport has the following fields
	//byr (Birth Year)
	//iyr (Issue Year)
	//eyr (Expiration Year)
	//hgt (Height)
	//hcl (Hair Color)
	//ecl (Eye Color)
	//pid (Passport ID)
	//cid (Country ID)
	
	protected String _byr;
	protected String _iyr;
	protected String _eyr;
	protected String _hgt;
	protected String _hcl;
	protected String _ecl;
	protected String _pid;
	protected String _cid;

	public boolean isPassport() {
		
		return _byr!= null && _iyr != null && _eyr != null && _hgt!=null && _hcl != null && _ecl != null && _pid !=null ;
	}
	protected void initializeFromString(String definition) {
		String attrs[] = definition.split("[ \n]");
		
		for (String attribute : attrs) {
			String keyVal[] = attribute.split(":");
			
			switch (keyVal[0]) {
			case "byr":
				_byr = keyVal[1];
				break;
			case "iyr":
				_iyr = keyVal[1];
				break;
			case "eyr":
				_eyr = keyVal[1];
				break;
			case "hgt":
				_hgt = keyVal[1];
				break;
			case "hcl":
				_hcl = keyVal[1];
				break;
			case "ecl":
				_ecl = keyVal[1];
				break;
			case "pid":
				_pid = keyVal[1];
				break;
			case "cid":
				_cid = keyVal[1];
				break;

			default:
				logger.error("Unexpected Attribute Name "+keyVal[0]);
				break;
			}
			
		}
	}
	@Override
	public String toString() {
		return "IdentityDocument [_byr=" + _byr + ", _iyr=" + _iyr + ", _eyr=" + _eyr + ", _hgt=" + _hgt + ", _hcl="
				+ _hcl + ", _ecl=" + _ecl + ", _pid=" + _pid + ", _cid=" + _cid + "]";
	}
	
	static protected boolean isYearValid(String year, int min, int max) {
		boolean valid = true;
		if (year.matches("[0-9][0-9][0-9][0-9]")) {
			int parseYear = Integer.parseInt(year);
			valid = parseYear >=min && parseYear <= max; 
		} else {
			valid=false;
		}
		return valid;
	}
	
	static protected boolean isHeightFormatValid(String height) {
		boolean valid = true;
		if (height.matches("[0-9]+((in)|(cm))")) {
			valid = true; 
		} else {
			valid=false;
		}
		return valid;
	}
	public static boolean isHeightValid(String height) {
		boolean retval = true;
		if (isHeightFormatValid(height)) {
			String unit = height.substring(height.length()-2);
			int scalar = Integer.parseInt(height.substring(0, height.length()-2));
			
			
			retval = (unit.equals("cm") && (scalar >= 150 && scalar <= 193)) || (unit.equals("in") && (scalar >= 59 && scalar <= 76)) ;
			
		} else {
			retval = false;
		}
		
		return retval;
	}
	public static boolean isHairColourValid(String hairColour) {
		return hairColour.matches("#[0-9a-f]{6}");
		
	}
	public static boolean isEyeColourValid(String eyeCol) {
		//amb blu brn gry grn hzl oth
		return eyeCol.equals("amb") || eyeCol.equals("blu") ||eyeCol.equals("brn") ||eyeCol.equals("gry") ||eyeCol.equals("grn") ||eyeCol.equals("hzl") || eyeCol.equals("oth");
		
	}
	public static boolean isPassportNumberValid(String passportNumber) {
		
		return passportNumber.matches("[0-9]{9}");
	}
	
	protected boolean isFullValid() {
//		byr (Birth Year) - four digits; at least 1920 and at most 2002.
//		iyr (Issue Year) - four digits; at least 2010 and at most 2020.
//		eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
//		hgt (Height) - a number followed by either cm or in:
//			If cm, the number must be at least 150 and at most 193.
//			If in, the number must be at least 59 and at most 76.
//		hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
//		ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
//		pid (Passport ID) - a nine-digit number, including leading zeroes.		
		
		return _byr != null && isYearValid(_byr, 1920, 2002) &&
				_iyr != null && isYearValid(_iyr, 2010, 2020) &&
				_eyr != null && isYearValid(_eyr, 2020, 2030) &&
				_hgt != null && isHeightValid(_hgt) &&
				_hcl != null && isHairColourValid(_hcl ) &&
				_ecl != null && isEyeColourValid(_ecl) && 
				_pid != null && isPassportNumberValid(_pid);
	}

}
