package aoc3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Day9Encoder {
	static final Logger logger = LogManager.getLogger("Day9Encoder");
	CircularBufferOfLongs _buffer;
	int _totalWritten = 0;
	
	Day9Encoder(int size) {
		_buffer = new CircularBufferOfLongs(size);
		
	}

	public static void main(String[] args) {
		logger.info("Starting Day 9");
		Day9Encoder encoder = new Day9Encoder(25);
		
		ArrayList<Long> lines = new ArrayList<>();

		try {
			File f = new File("D:\\temp\\encoder.txt");
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			while ((readLine = b.readLine()) != null) {
				if (readLine.length() > 0) {
					Long readNumber = Long.parseLong(readLine.trim());
					lines.add(readNumber);

					if (!encoder.loadLine(readNumber)) {
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		int lineNum = lines.size()-1;
		logger.info("Broken Value Ending First Loop is "+lines.get(lineNum)+" on line "+lineNum);
		
		long brokenValue = lines.get(lineNum);
		
		//At this point lines is the list of everything thus far
		

		
		int sequenceLength = 2;

		boolean finished = false;
		
		while (!finished) {
			encoder = new Day9Encoder(sequenceLength);
			for (Long readNumber : lines) {
				encoder.loadLine(readNumber);
				if (encoder._buffer._sum == brokenValue) {
					finished = true;
					break;
				}
			}
			if (!finished) {
				logger.debug("Not found at length "+sequenceLength);
				sequenceLength++;
			}
		}
		logger.info("Crypto Weakness "+encoder._buffer.getSumOfSmallestAndBiggest());
		

	}

	protected boolean loadLine(Long num) {
		boolean isValid = true;
		if (_totalWritten < _buffer._capacity) {
			// No checking on preamble
			_buffer.write(num);
			_totalWritten++;  // Move to buffer
		} else {
			logger.info("Value <"+num+"> is "+isValueValid(num));
			if (!isValueValid(num)) {
				logger.warn("INVALID VALUE IS "+num);
				isValid = false;
			}
			_buffer.write(num);
			_totalWritten++;
		}
		return isValid;
	}
	

	public boolean isValueValid(Long num) {
		// Value is valid if it is the sum of two (different) numbers already in the buffer
		ArrayList<Long> buf = _buffer.getCurrentContents();
		boolean valid = false;
		for(int i = 0; i< buf.size(); i++) {
			long testItem = buf.get(i);
			long lookingFor = num - testItem;
			if (lookingFor == num) {
				if (buf.indexOf(lookingFor) != -1 && buf.indexOf(lookingFor) != buf.lastIndexOf(lookingFor)) {
					valid = true;
				}
			} else {
				if (buf.indexOf(lookingFor) > -1 ) {
					valid = true;
				}
			}
			if (valid) break;
		}
		return valid;
	}

}
