package aoc3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import aoc3.Day10Joltage.Traversal;

public class JoltageList {
	static final Logger logger = LogManager.getLogger("Joltages");

	public class Traversal {
		public Traversal(int frm, int to) {
			_fromIndex = frm;
			_toIndex = to;
			_fromJolt = _availableDevices.get(_fromIndex);
			_toJolt = _availableDevices.get(to);

		}

		int _fromIndex;
		int _fromJolt;
		int _toIndex;
		int _toJolt;
	}

	protected Set<Traversal> _possibleTraversals;
	protected List<Integer> _availableDevices;

	public JoltageList(List joltages) {
		Collections.sort(joltages);
		_availableDevices = new ArrayList<>();
		_availableDevices.add(0);
		_availableDevices.addAll(joltages);
		_availableDevices.add(_availableDevices.get(_availableDevices.size()-1)+3);
		_possibleTraversals = new HashSet<Traversal>();

	}

	protected void populateTraversals() {
		// Start at position 1 and work forward
		for (int i = 1; i < _availableDevices.size()-1; i++) {
			// So, in order to work out how many routes there are to this position
			int thisDeviceJoltage = _availableDevices.get(i);

			for (int backwards = i - 1; backwards >= 0; backwards--) {
				if (_availableDevices.get(backwards) >= thisDeviceJoltage - 3) {
					Traversal t = new Traversal(backwards, i);
					_possibleTraversals.add(t);
				}
			}
		}
	}

	protected int countTraversals() {
		// So, we need to know how many items there are between us and the item3 before
		int currentPosition = 0;
		int numberOfItemsbetween =  0;
		int furthestInterestingIndex =0;
		int ret=1;
		while (furthestInterestingIndex != _availableDevices.size()-1) {
			int myJoltage = _availableDevices.get(currentPosition);
			furthestInterestingIndex = !(_availableDevices.lastIndexOf(myJoltage - 3) == -1)
					? _availableDevices.lastIndexOf(myJoltage - 3)
					: !(_availableDevices.lastIndexOf(myJoltage - 2) == -1) ? _availableDevices.lastIndexOf(myJoltage - 2)
							: !(_availableDevices.lastIndexOf(myJoltage - 1) == -1)
									? _availableDevices.lastIndexOf(myJoltage - 1)
									: -1;
			logger.debug("At pos "+currentPosition+" list Len "+_availableDevices.size()+" last interesting = "+furthestInterestingIndex);

			if (furthestInterestingIndex == -1) {
				logger.error("Impossible List");
			} else {
				numberOfItemsbetween = furthestInterestingIndex - currentPosition;
			}
			currentPosition++;
			logger.debug("At pos "+currentPosition+" list Len "+_availableDevices.size()+" last interesting = "+furthestInterestingIndex);
			 ret *= (int)Math.pow(2,numberOfItemsbetween-1);
			logger.debug("At pos "+currentPosition+" Routes "+ret);
			 
		}

		return ret;
	}

	public ArrayList<ArrayList<Integer>> getSubparts() {
		ArrayList<ArrayList<Integer>> ret;
		ret = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> thisSeq= new ArrayList<>();
		thisSeq.add(_availableDevices.get(0));
		
		for(int i=1; i<_availableDevices.size();i++) {
			if ( _availableDevices.get(i)-_availableDevices.get(i-1) ==3) {
				ret.add(thisSeq);
				thisSeq = new ArrayList<>();
				thisSeq.add(_availableDevices.get(i));
			} else {
				
				thisSeq.add(_availableDevices.get(i));
			}
		}
		return ret;
	}
	
	
	

}
