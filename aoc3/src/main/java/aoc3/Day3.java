/**
 * 
 */
package aoc3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * @author George
 *
 */
public class Day3 {
	  static final Logger logger = LogManager.getLogger("Day3");
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		logger.info("Starting");
		TreeMap map = new TreeMap();
		map.initializeFromFile("D:\\Temp\\map.txt");
//		Instructions are to use these as routes
//		Right 1, down 1.
//		Right 3, down 1. (This is the slope you already checked.)
//		Right 5, down 1.
//		Right 7, down 1.
//		Right 1, down 2.
		
		long trees = map.countTreesOnRouteThrough(1,1);
		trees *= map.countTreesOnRouteThrough(3, 1);
		trees *= map.countTreesOnRouteThrough(5, 1);
		trees *= map.countTreesOnRouteThrough(7,1);
		trees *= map.countTreesOnRouteThrough(1,2);
		
		System.out.println("Trees on route "+trees);
	}

}
