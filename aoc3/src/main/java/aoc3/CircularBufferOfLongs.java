package aoc3;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CircularBufferOfLongs
{
	static final Logger logger = LogManager.getLogger("Day9Encoder");

	// I was going to use a native array here but 
	// generics are odd in java so I can't do that easily
	protected ArrayList<Long> _buffer;
	protected int _curSize = 0;
	protected int _capacity;
	protected int _writePos;
	protected int _readPos;
	protected long _sum;
	
	CircularBufferOfLongs(int size) {
		_buffer = new ArrayList<>();
		_capacity = size;
		_writePos = 0;
		_readPos = 0;
		_curSize = 0;
		_sum =0;
	}


	public void write(Long item) {
		if (_writePos >= _buffer.size()) {
			_buffer.add(item);
			_writePos = (_writePos+1) % _capacity;
			_sum+=item;
		} else {
			_sum-=_buffer.get(_writePos);			
			_buffer.set(_writePos, item);
			_writePos = (_writePos+1) % _capacity;
			_sum += item;
		}
		logger.debug("Sum is now "+_sum);
	}
	
	public Long read() {
		Long item = _buffer.get(_readPos);
		_readPos = (_readPos +1 ) % _capacity;
		return item;
	}
	
	public long getSumOfSmallestAndBiggest() {
		ArrayList<Long> temp = new ArrayList();
		temp.addAll(_buffer);
		Collections.sort(temp);
		return temp.get(0)+temp.get(temp.size()-1);
	}
	
	protected ArrayList<Long> getCurrentContents() {
		ArrayList<Long> ret = new ArrayList<>();
		ret.addAll(_buffer);
		return ret;
	}

}
