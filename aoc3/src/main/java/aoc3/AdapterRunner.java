package aoc3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdapterRunner {

	static final Logger logger = LogManager.getLogger("Joltages");
	protected static Day10Joltage _seatAdapter;
	protected static Day10Joltage getSeatAdapter() {
		if (_seatAdapter == null) {
			_seatAdapter = new Day10Joltage(0);
		}
		return _seatAdapter;
	}
	
	
	public static void main(String[] args) {
		
		Day10Joltage seatAdapter = getSeatAdapter();
		ArrayList<Integer> joltages = new ArrayList();
		
		try {
			File f = new File("D:\\temp\\Day10Adapters.txt");
			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";
			while ((readLine = b.readLine()) != null) {
				if (readLine.length() > 0) {
					joltages.add(Integer.parseInt(readLine.trim()));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		Collections.sort(joltages);
		seatAdapter._remainingAdapters = joltages;
		seatAdapter.processRemaining();
		
	}

}
