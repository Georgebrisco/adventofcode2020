package aoc3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Day4 {
	static final Logger logger = LogManager.getLogger("Day3");
	public static void main(String[] args) {
		int validCount = 0;
		int totalCount = 0;
		
		logger.info("Starting Day4");
			try {
            File f = new File("D:\\temp\\identifications.txt");
            BufferedReader b = new BufferedReader(new FileReader(f));
            String identification = "";
 
            String readLine = "";
            while ((readLine = b.readLine()) != null) {
            	if (readLine.length()==0) {
            		logger.debug("New Line. Next id");
            		IdentityDocument doc = new IdentityDocument();
            		doc.initializeFromString(identification.trim());
            		identification = "";
            		logger.debug(doc);
            		totalCount++;
            		if (doc.isFullValid()) {
            			validCount++;
            		}
            	} else {
            		identification += " "+readLine;
            	}
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
		System.out.println("Out of "+totalCount+" documents "+validCount+" were valid passports");
	}
}
