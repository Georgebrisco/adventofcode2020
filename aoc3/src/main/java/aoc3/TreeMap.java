package aoc3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TreeMap {
	private ArrayList<ArrayList<Boolean>> _map = new ArrayList<ArrayList<Boolean>>();
	
	protected static class  RouteMove {
		protected int _xmove;
		protected int _ymove;
		
		protected RouteMove(int x, int y) {
			_xmove =x;
			_ymove = y;
		}
	};
	
	static final Logger logger = LogManager.getLogger("Day3");
	
	
	protected boolean isThereATreeAtPOsition(int x, int y) {
		return (_map.get(y).get(x));
	}
	
	protected int countTreesOnRouteThrough(int xMove, int yMove) {
		int treeCount = 0;		
		int xSize = _map.get(0).size();
		int x=0;
		int y=0;
		
		while (y < _map.size()) {
			logger.info("Checking Position "+x+","+y);
			treeCount = isThereATreeAtPOsition(x, y) ? treeCount+1 : treeCount;
			x = (x+xMove) % xSize;
			y += yMove ;
		}
		return treeCount;
	}
	
	
	protected int getNumberOfTrees() {
		int count = 0;
		for (ArrayList<Boolean> mapLine : _map) {
			for (Boolean tree : mapLine) {
				count = tree ? count+1 : count;
			}		
		}
		return count;
	}
	
	
	protected void initializeFromFile(String fileName) {
		int x,y=0;
		
		try {
            File f = new File(fileName);
            BufferedReader b = new BufferedReader(new FileReader(f));
            String readLine = "";
            while ((readLine = b.readLine()) != null) {
            	ArrayList<Boolean> line = new ArrayList<Boolean>();
                logger.info(readLine);            	
            	for (char ch: readLine.toCharArray()) {
            		if (ch=='#') {
            			// It is a tree
            			line.add(true);
            		} else {
            			line.add(false);
            		}
            	}
            	_map.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	protected void initializeFromStringArray(String[] lines) {
		for (String mapline : lines) {
          	ArrayList<Boolean> line = new ArrayList<Boolean>();
            logger.info(mapline);            	
        	for (char ch: mapline.toCharArray()) {
        		if (ch=='#') {
        			// It is a tree
        			line.add(true);
        		} else {
        			line.add(false);
        		}
        	}
        	_map.add(line);
		}
	}
}
