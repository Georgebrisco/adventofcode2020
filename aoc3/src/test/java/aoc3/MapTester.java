package aoc3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MapTester {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void initializeMapAndCountTrees() {
		TreeMap testMap = new TreeMap();
		String[] inpLines = new String[] {
				"..##.........##.........##.........##.........##.........##.......",
				"#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..",
				".#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.",
				"..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#",
				".#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.",
				"..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....",
				".#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#",
				".#........#.#........#.#........#.#........#.#........#.#........#",
				"#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...",
				"#...##....##...##....##...##....##...##....##...##....##...##....#",
				".#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.# "				
		};
		
		testMap.initializeFromStringArray(inpLines);
		assert(testMap.getNumberOfTrees()==222);

	}
	
	@Test
	void isThereATreeAt() {
		TreeMap testMap = new TreeMap();
		String[] inpLines = new String[] {
				"..##.........##.........##.........##.........##.........##.......",
				"#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..",
				".#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.",
				"..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#",
				".#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.",
				"..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....",
				".#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#",
				".#........#.#........#.#........#.#........#.#........#.#........#",
				"#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...",
				"#...##....##...##....##...##....##...##....##...##....##...##....#",
				".#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.# "				
		};
		
		testMap.initializeFromStringArray(inpLines);
		assert(testMap.isThereATreeAtPOsition(0, 0)==false);
		assert(testMap.isThereATreeAtPOsition(0, 1)==true);
		assert(testMap.isThereATreeAtPOsition(1, 0)==false);
		

	}
	
	@Test
	void testSimpleRoute() {
		TreeMap testMap = new TreeMap();
		String[] inpLines = new String[] {
				"..##.........##.........##.........##.........##.........##.......",
				"#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..",
				".#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.",
				"..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#",
				".#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.",
				"..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....",
				".#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#",
				".#........#.#........#.#........#.#........#.#........#.#........#",
				"#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...",
				"#...##....##...##....##...##....##...##....##...##....##...##....#",
				".#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.# "				
		};
		
		testMap.initializeFromStringArray(inpLines);
		assert(testMap.countTreesOnRouteThrough(0, 1)==3);

		

	}
	
	@Test
	void testTestRoute() {
		TreeMap testMap = new TreeMap();
		String[] inpLines = new String[] {
				"..##.........##.........##.........##.........##.........##.......",
				"#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..",
				".#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.",
				"..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#",
				".#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.",
				"..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....",
				".#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#",
				".#........#.#........#.#........#.#........#.#........#.#........#",
				"#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...",
				"#...##....##...##....##...##....##...##....##...##....##...##....#",
				".#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.# "				
		};
		
		testMap.initializeFromStringArray(inpLines);
		assert(testMap.countTreesOnRouteThrough(1, 1)==2);
		assert(testMap.countTreesOnRouteThrough(3, 1)==7);
		assert(testMap.countTreesOnRouteThrough(5, 1)==3);
		assert(testMap.countTreesOnRouteThrough(7, 1)==4);
		assert(testMap.countTreesOnRouteThrough(1, 2)==2);

		

	}

	

	
	@Test
	void testReallySimpleRoute() {
		TreeMap testMap = new TreeMap();
		String[] inpLines = new String[] {
				"##",
				"##"		};
		
		testMap.initializeFromStringArray(inpLines);
		assert(testMap.isThereATreeAtPOsition(0, 0));
		assert(testMap.isThereATreeAtPOsition(0, 1));
		assert(testMap.isThereATreeAtPOsition(1, 0));
		assert(testMap.isThereATreeAtPOsition(1, 1));

		assert(testMap.countTreesOnRouteThrough(0, 1)==2);

		

	}
	@Test
	void testReallySimpleRoutew() {
		TreeMap testMap = new TreeMap();
		String[] inpLines = new String[] {
				"#.",
				".."		};
		
		testMap.initializeFromStringArray(inpLines);
		assert(testMap.isThereATreeAtPOsition(0, 0));
		assert(testMap.isThereATreeAtPOsition(0, 1)==false);
		assert(testMap.isThereATreeAtPOsition(1, 0)==false);
		assert(testMap.isThereATreeAtPOsition(1, 1)==false);

		assert(testMap.countTreesOnRouteThrough(0, 1)==1);

		

	}


}
