package aoc3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class Day6Tests {

	@Test
	void testGetStringWithUniqueElements() {
		assert(Day6.getStringWithUniqueElements("abc").equals("abc"));
		assert(Day6.getStringWithUniqueElements("abcdeef").equals("abcdef"));
		assert(Day6.getStringWithUniqueElements("abc\r\ndef").equals("abcdef"));
		
	}

	@Test
	void exampleTest() {

		String inp = "abc\r\n"
				+ "\r\n"
				+ "a\r\n"
				+ "b\r\n"
				+ "c\r\n"
				+ "\r\n"
				+ "ab\r\n"
				+ "ac\r\n"
				+ "\r\n"
				+ "a\r\n"
				+ "a\r\n"
				+ "a\r\n"
				+ "a\r\n"
				+ "\r\n"				
				+ "\r\n"
				+ "b"
				+ "\r\n"
				+ "\r\n"
				+ "\r\n"
				+ "\r\n";
		
		System.out.println(inp);
		
		String lines[] = inp.split("\r\n");
		String answers = "";
		int totalCount =0;
		int validCount = 0;

		int total =0;
		int maxScore = 0;
		int now = 0;
		
		for (String readLine : lines) {
			now++;
			System.out.println("ReadLine is "+readLine);
			
        	if (readLine.length()==0 || now >= lines.length ) {
        		if (now == lines.length) {
        			answers += readLine;
        		}
        		System.out.print("answers is "+answers);
				total += Day6.getStringWithUniqueElements(answers).length();
				maxScore = Day6.getStringWithUniqueElements(answers).length() > maxScore ? Day6.getStringWithUniqueElements(answers).length() : maxScore;
				System.out.println("Score was "+Day6.getStringWithUniqueElements(answers).length());
				answers = "";
        	} else {
        		answers += " "+readLine;
        	}
		}
		
		System.out.println("Total Score is "+total);
		assert(total == 11);
	}
	
	
	@Test
	void checkAnswerSet() {
		Day6AnswerSet as = new Day6AnswerSet("a a a abc a");
		assert(as._individualAnswers.length == 5);
		assert(as._individualAnswers[3].equals("abc"));
		
	}
	
	@Test
	void checkShortestOne() {
		Day6AnswerSet as = new Day6AnswerSet("a a a abc a");
		assert(as.getOneOfShortestStrings().equals("a"));
		as = new Day6AnswerSet("a ab abc abcd");
		assert(as.getOneOfShortestStrings().equals("a"));
		as = new Day6AnswerSet("aa ab abc abcd a");
		assert(as.getOneOfShortestStrings().equals("a"));
	}
	
	@Test
	void checkGetCommonAnswers() {
		Day6AnswerSet as = new Day6AnswerSet("a a a abc a");
		String answers = as.getCommonAnswers();
		assert(answers.length() == 1);
		assert(answers.contains("a"));
	}
	@Test
	void checkGroupParsing1() {
		Day6AnswerSet as = new Day6AnswerSet("abc");
		String answers = as.getCommonAnswers();
		assert(answers.length() == 3);
		assert(answers.contains("abc"));
	}

	@Test
	void checkGroupParsing2() {
		Day6AnswerSet as = new Day6AnswerSet("a b c");
		String answers = as.getCommonAnswers();
		assert(answers.length() == 0);
		
	}

	@Test
	void checkGroupParsing3() {
		Day6AnswerSet as = new Day6AnswerSet("ab bc");
		String answers = as.getCommonAnswers();
		assert(answers.length() == 1);
		assert(answers.contains("b"));
	}
	@Test
	void checkGroupParsing4() {
		Day6AnswerSet as = new Day6AnswerSet("a a a a");
		String answers = as.getCommonAnswers();
		assert(answers.length() == 1);
		assert(answers.contains("a"));
	}
	
	@Test
	void checkGroupParsing5() {
		Day6AnswerSet as = new Day6AnswerSet("b");
		String answers = as.getCommonAnswers();
		assert(answers.length() == 1);
		assert(answers.contains("b"));
	}
	
	
}
