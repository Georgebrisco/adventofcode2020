package aoc3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ComputerTester {

	@Test
	void testLoad() {
		String[] program = {
				"nop +0",
				"acc +1",
				"jmp +4",
				"acc +3",
				"jmp -3",
				"acc -99",
				"acc +1",
				"jmp -4",
				"acc +6" };
		
		Day7Computer computer = new Day7Computer();
		computer.loadProgram(program);

		assert(computer._memory.get(11).equals("-99"));
		

	}
	
	@Test
	void testRun() {
		String[] program = {
				"nop +0",
				"acc +1",
				"jmp +4",
				"acc +3",
				"jmp -3",
				"acc -99",
				"acc +1",
				"jmp -4",
				"acc +6" };
		
		Day7Computer computer = new Day7Computer();
		computer.loadProgram(program);
		computer.runProgram();
		assert(computer._accumulator == 5);
	}

	@Test
	void testStandardExitWorks() {
		String[] program = {
				"nop +0",
				"acc +1",
				 };
		
		Day7Computer computer = new Day7Computer();
		computer.loadProgram(program);
		computer.runProgram();
		assert(computer._accumulator == 1);
		assert(computer._instructionPointer == 4);
	}
	
	@Test
	void lookFromTester() {
		String[] program = {
				"nop +0",
				"acc +1",
				"jmp +4",
				"acc +3",
				"jmp -3",
				"acc -99",
				"acc +1",
				"jmp -4",
				"acc +6" };
		
		Day7Computer computer = new Day7Computer();
		computer.loadProgram(program);
		int pos = 0;
		pos = computer.getNopOrJmpAfter(pos);
		assert(pos==0);
		pos++;
		pos = computer.getNopOrJmpAfter(pos);
		assert(pos==4);
		pos++;
		pos = computer.getNopOrJmpAfter(pos);
		assert(pos==8);
		pos++;
		pos = computer.getNopOrJmpAfter(pos);
		assert(pos==14);
		pos++;
		pos = computer.getNopOrJmpAfter(pos);
		assert(pos==18);
		pos++;
		
	}

	
	
}
