package aoc3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import org.junit.jupiter.api.Test;

class BagTester {

	@Test
	void testSimple() {
		BagDetails testBag = new BagDetails("blue");
		assert(testBag.getColour().equals("blue"));
		assert(testBag.getNumberOfContainedColours() == 0);
		assert(testBag.getNumberOfPossibleImmediateContainers() == 0);

	}

	@Test
	void singleRuleTest() {
		String[] ruleSet = {
				"light red bags contain 1 bright white bag, 2 muted yellow bags."
		};
		
		BagRuleProcessor processor = new BagRuleProcessor();
		processor.processRuleSet(ruleSet);
		
		BagDetails lightRed = processor.getBag("light red");
		assert(lightRed.getColour().equals("light red"));
		assert(lightRed.getNumberOfContainedColours() == 2);
		assert(lightRed.getNumberOfPossibleImmediateContainers() == 0);
		
		BagDetails mutedYellow = processor.getBag("muted yellow");
		assert(mutedYellow.getColour().equals("muted yellow"));
		assert(mutedYellow.getNumberOfContainedColours() == 0);
		assert(mutedYellow.getNumberOfPossibleImmediateContainers() == 1);
	}
	
	@Test
	void DoubleRuleTest() {
		String[] ruleSet = {
				"light red bags contain 1 bright white bag, 2 muted yellow bags.",
				"muted yellow bags contain 1 bright green bag"
		};
		
		BagRuleProcessor processor = new BagRuleProcessor();
		processor.processRuleSet(ruleSet);
		
		BagDetails lightRed = processor.getBag("light red");
		assert(lightRed.getColour().equals("light red"));
		assert(lightRed.getNumberOfContainedColours() == 2);
		assert(lightRed.getNumberOfPossibleImmediateContainers() == 0);
		
		BagDetails mutedYellow = processor.getBag("muted yellow");
		assert(mutedYellow.getColour().equals("muted yellow"));
		assert(mutedYellow.getNumberOfContainedColours() == 1);
		assert(mutedYellow.getNumberOfPossibleImmediateContainers() == 1);
	}
	
	@Test
	void multiRuleBagRulesTest() {
		String[] ruleSet = {
				"light red bags contain 1 bright white bag, 2 muted yellow bags.",
				"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
				"bright white bags contain 1 shiny gold bag.",
				"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
				"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
				"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
				"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
				"faded blue bags contain no other bags.",
				"dotted black bags contain no other bags."
		};
		
		BagRuleProcessor processor = new BagRuleProcessor();
		processor.processRuleSet(ruleSet);
		
		assert(processor._allBags.keySet().size()==9);

		BagDetails dottedBlack = processor.getBag("dotted black" );
		assert(dottedBlack.getNumberOfContainedColours() == 0);
		assert(dottedBlack.getNumberOfPossibleImmediateContainers() == 2);
	}
	
	@Test
	void simpleParentTest() {
		String[] ruleSet = {
				"light red bags contain 1 bright white bag, 2 muted yellow bags.",
				"light red bags contain no other bags."
		};
		
		BagRuleProcessor processor = new BagRuleProcessor();
		processor.processRuleSet(ruleSet);
		
		assert(processor._allBags.keySet().size()==3);

		BagDetails brightWhite = processor.getBag("bright white" );
		assert(brightWhite.getAllParents().size() == 1);
		assert(brightWhite.getNumberOfPossibleImmediateContainers() == 1);
	}
	
	@Test
	void complexParentTest() {
		String[] ruleSet = {
				"light red bags contain 1 bright white bag, 2 muted yellow bags.",
				"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
				"bright white bags contain 1 shiny gold bag.",
				"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
				"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
				"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
				"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
				"faded blue bags contain no other bags.",
				"dotted black bags contain no other bags."
		};
		
		BagRuleProcessor processor = new BagRuleProcessor();
		processor.processRuleSet(ruleSet);
		
		assert(processor._allBags.keySet().size()==9);

		BagDetails shinygold = processor.getBag("shiny gold" );
		LinkedHashSet<BagDetails> pars = shinygold.getAllParents();
		assert(shinygold.getAllParents().size() == 4);
	}
	
	@Test 
	void testCountingEnclosedBags() {
		String[] ruleSet = {
				"light red bags contain 1 bright white bag, 2 muted yellow bags.",
		};
		
		BagRuleProcessor processor = new BagRuleProcessor();
		processor.processRuleSet(ruleSet);
		
		assert(processor._allBags.keySet().size()==3);

		BagDetails lightred = processor.getBag("light red" );
		int enclosed = lightred.enclosedCount();
		assert(enclosed == 3);
		
	}
	
	
	@Test 
	void testCountingEnclosedBagsExample() {
		String[] ruleSet = {
				"shiny gold bags contain 2 dark red bags.",
				"dark red bags contain 2 dark orange bags.",
				"dark orange bags contain 2 dark yellow bags.",
				"dark yellow bags contain 2 dark green bags.",
				"dark green bags contain 2 dark blue bags.",
				"dark blue bags contain 2 dark violet bags.",
				"dark violet bags contain no other bags."		};
		
		BagRuleProcessor processor = new BagRuleProcessor();
		processor.processRuleSet(ruleSet);
		


		BagDetails shinygold = processor.getBag("shiny gold" );
		int enclosed = shinygold.enclosedCount();
		assert(enclosed == 126);
		
	}
	

	
	
	
	//	@Test
//	void multiSimpleDirectContentsTest() {
//		String[] ruleSet = {
//				"light red bags contain 1 bright white bag, 2 muted yellow bags."
//		};
//		
//		BagRules rules = new BagRules();
//		for (String rule : ruleSet) {
//			rules.processRule(rule);
//		}
//		
//		
//		BagDetails testBag = rules._bags.get("bright white");
//		ArrayList<BagDetails> containers = testBag.getListOfBagsThatDirectlynContainMe();
//		assert(containers.size() == 1);
//		assert(containers.get(0)._bagColour.equals("light red"));
//	}
//
//	

	
}
