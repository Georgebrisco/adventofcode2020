package aoc3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.print.attribute.standard.JobHoldUntil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

class JoltageTester {
	static final Logger logger = LogManager.getLogger("Joltages");

	@Test
	void testcalculateDevicePowerRating() {
		logger.debug("testcalculateDevicePowerRating");
		List<Integer> joltages = Arrays.asList(1, 16, 10, 15, 5, 11, 7, 19, 6, 12, 4);
		Day10Joltage seatAdapter = new Day10Joltage(0);
		seatAdapter.addAvailableAdapterValues(joltages);
		ArrayList<Integer> ads = seatAdapter.getBiggestValidListIncludingThisAndDevice();
		assert (ads.get(ads.size() - 1) == 22);
	}

	@Test
	void simpleDifferences() {
		logger.debug("testcalculateDevicePowerRating");
		List<Integer> joltages = Arrays.asList(1, 16, 10, 15, 5, 11, 7, 19, 6, 12, 4);
		Day10Joltage seatAdapter = new Day10Joltage(0);
		seatAdapter.addAvailableAdapterValues(joltages);
		ArrayList<Integer> ads = seatAdapter.getBiggestValidListIncludingThisAndDevice();
		int numOnes = Day10Joltage.getNumberOfDiffs(ads, 1);
		int numThrees = Day10Joltage.getNumberOfDiffs(ads, 3);

		assert (numOnes == 7);
		assert (numThrees == 5);

	}

	@Test
	void testTraversalSingle() {
		logger.debug("testTraversalSingle");
		List<Integer> joltages = Arrays.asList(1, 4);
		JoltageList jl = new JoltageList(joltages);
		jl.populateTraversals();
		int traversals = jl.countTraversals();
		assert (traversals == 1);

	}

	@Test
	void testTraversalGapTooBig() {
		logger.debug("testTraversalSingleTooBig");
		List<Integer> joltages = Arrays.asList(1, 5);
		JoltageList jl = new JoltageList(joltages);
		jl.populateTraversals();
		int traversals = jl.countTraversals();
		assert (traversals == 0);

	}

@Test
void testTraversalWithInterdiary() {
	logger.debug("testTraversalOneIntermediary");
	List<Integer> joltages = Arrays.asList(1,2,3);
	JoltageList jl = new JoltageList(joltages);
	jl.populateTraversals();
	int traversals = jl.countTraversals();
	assert (traversals == 3);

}

@Test
void testTraversalsCase1() {
	logger.debug("testTraversalsCase1");
	List<Integer> joltages = Arrays.asList(10,7);
	JoltageList jl = new JoltageList(joltages);
	jl.populateTraversals();
	int traversals = jl.countTraversals();
	assert (traversals == 1);

}

@Test
void testTraversalsCase2() {
	logger.debug("testTraversalsCase2");
	List<Integer> joltages = Arrays.asList(10,9,7);
	JoltageList jl = new JoltageList(joltages);
	jl.populateTraversals();
	int traversals = jl.countTraversals();
	assert (traversals == 2);

}

@Test
void testTraversalsCase3() {
	logger.debug("testTraversalsCase3");
	List<Integer> joltages = Arrays.asList(10,9,8,7);
	JoltageList jl = new JoltageList(joltages);
	jl.populateTraversals();
	int traversals = jl.countTraversals();
	assert (traversals == 4);

}

@Test
void testTraversalsCase4() {
	logger.debug("testTraversalsCase4");
	List<Integer> joltages = Arrays.asList(11,8,7,6,5);
	JoltageList jl = new JoltageList(joltages);
	jl.populateTraversals();
	int traversals = jl.countTraversals();
	assert (traversals == 4);

}

@Test
void testTraversalsCase5() {
	logger.debug("testTraversalsCase5");
	List<Integer> joltages = Arrays.asList(1,4,6,8,10,13);
	JoltageList jl = new JoltageList(joltages);
	jl.populateTraversals();
	int traversals = jl.countTraversals();
	assert (traversals == 4);

}

@Test void testGettingSubParts() {
	logger.debug("test subparks");
	List<Integer> joltages = Arrays.asList(15,12,11,10,7,4,3,2,1);
	JoltageList jl = new JoltageList(joltages);
	ArrayList<ArrayList<Integer>> subParts = jl.getSubparts();
	assert (subParts.size() == 3);
}

@Test void testGettingSubParts2() {
	logger.debug("test subparks");
	List<Integer> joltages = Arrays.asList(28,33,18,42,31,14,46,20,48,47,24,23,49,45,19,38,39,11,1,32,25,35,8,17,7,9,4,2,34,10,3);
	JoltageList jl = new JoltageList(joltages);
	ArrayList<ArrayList<Integer>> subParts = jl.getSubparts();
	assert (subParts.size() == 9);
}

@Test void testGettingSubPartsReal() {
	logger.debug("test subparks");
	List<Integer> joltages = Arrays.asList(128,6,152,16,118,94,114,3,146,44,113,83,46,40,37,72,149,155,132,9,75,1,82,80,111,124,66,122,129,32,30,136,112,65,90,117,11,45,161,55,135,17,159,38,51,131,12,123,81,64,50,43,19,63,13,153,110,27,23,104,145,18,125,86,10,76,26,142,59,47,160,79,139,54,121,97,162,36,107,56,25,99,24,31,69,137,33,138,130,158,91,2,74,101,73,20,98,154,89,62,100,39);
	JoltageList jl = new JoltageList(joltages);
	ArrayList<ArrayList<Integer>> subParts = jl.getSubparts();
	//assert (subParts.size() == 30);
	long routes = 1;
	
	for (ArrayList<Integer> subPart : subParts) {
		logger.debug("Length of SubPart is "+subPart.size());
		logger.debug("Subpart is "+subPart);
		int spLen = subPart.size();
		if (spLen == 3) {
			routes *= 2;
		} else if (spLen == 4) {
			routes *= 4;
		} else if (spLen == 5 ) {
			routes *= 7;
		} 
		assert(subPart.size()<6);
	}
	logger.debug("Routes for Real is "+routes);

}

@Test void testGettingSubPartsExample() {
	logger.debug("Example SubParts");
	List<Integer> joltages = Arrays.asList(28,33,18,42,31,14,46,20,48,47,24,23,49,45,19,38,39,11,1,32,25,35,8,17,7,9,4,2,34,10,3);
	JoltageList jl = new JoltageList(joltages);
	ArrayList<ArrayList<Integer>> subParts = jl.getSubparts();
	//assert (subParts.size() == 30);
	int routes = 1;
	
	for (ArrayList<Integer> subPart : subParts) {
		logger.debug("Length of SubPart is "+subPart.size());
		logger.debug("Subpart is "+subPart);
		int spLen = subPart.size();
		if (spLen == 3) {
			routes *= 2;
		} else if (spLen == 4) {
			routes *= 4;
		} else if (spLen == 5 ) {
			routes *= 7;
		} 
		assert(subPart.size()<6);
	}
	logger.debug("Routes for Example is "+routes);
}



@Test
void testTraversalsFromExercise() {
	logger.debug("testTraversalsFromExercise");
	List<Integer> joltages = Arrays.asList(16,10,15,5,1,11,7,19,6,12,4);
	JoltageList jl = new JoltageList(joltages);
	jl.populateTraversals();
	int traversals = jl.countTraversals();
	assert (traversals == 9);
}

@Test
void testBigTraversalsExercise() {
	logger.debug("testBigTraversalsExercise");
	List<Integer> joltages = Arrays.asList(28,33,18,42,31,14,46,20,48,47,24,23,49,45,19,38,39,11,1,32,25,35,8,17,7,9,4,2,34,10,3);
	JoltageList jl = new JoltageList(joltages);
	int traversals = jl.countTraversals();
	assert (traversals == 19208);
	
}

	@Test
	void testAcceptance2() {
		Day10Joltage adapter1 = new Day10Joltage(1);
		Day10Joltage examined = new Day10Joltage(4);
		assert (examined.canBePluggedInto(adapter1));

	}

	@Test
	void testAcceptance3() {
		Day10Joltage adapter1 = new Day10Joltage(0);
		Day10Joltage examined = new Day10Joltage(1);
		assert (examined.canBePluggedInto(adapter1));

	}

	@Test
	void testPluggingIn() {
		String[] smallJoltageList = { "16", "10", "15", "5", "1", "11", "7", "19", "6", "12", "4" };

		Day10Joltage seatAdapter = new Day10Joltage(0);

		for (String joltage : smallJoltageList) {
			int jlt = Integer.parseInt(joltage);
			Day10Joltage examined = new Day10Joltage(jlt);
			if (examined.canBePluggedInto(seatAdapter)) {
				examined.plugInto(seatAdapter);
			}
		}

		assert (seatAdapter._pluggedIntoMe.get(0)._joltage == 1);

	}

}
