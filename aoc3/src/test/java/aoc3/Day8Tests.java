package aoc3;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

class Day8Tests {
	static final Logger logger = LogManager.getLogger("Day9Encoder");

	@Test
	void testCircBufferNoWrapAround() {
		CircularBufferOfLongs testBuf = new CircularBufferOfLongs(5);
		testBuf.write(Long.valueOf(1));
		testBuf.write(Long.valueOf(2));
		testBuf.write(Long.valueOf(3));
		testBuf.write(Long.valueOf(4));
		testBuf.write(Long.valueOf(5));

		
		assert(testBuf.read() == 1);
		assert(testBuf.read() == 2);
		assert(testBuf.read() == 3);
		assert(testBuf.read() == 4);
		assert(testBuf.read() == 5);
		
	}
	@Test
	void testCircBufferWrapAround() {
		CircularBufferOfLongs testBuf = new CircularBufferOfLongs(5);
		testBuf.write(Long.valueOf(1));
		testBuf.write(Long.valueOf(2));
		testBuf.write(Long.valueOf(3));
		testBuf.write(Long.valueOf(4));
		testBuf.write(Long.valueOf(5));
		testBuf.write(Long.valueOf(6));

		
		assert(testBuf.read() == 6);
		assert(testBuf.read() == 2);
		assert(testBuf.read() == 3);
		assert(testBuf.read() == 4);
		assert(testBuf.read() == 5);
		
	}
	
	@Test
	void testCircBufferNonWrapTotal() {
		CircularBufferOfLongs testBuf = new CircularBufferOfLongs(5);
		testBuf.write(Long.valueOf(1));
		testBuf.write(Long.valueOf(2));
		testBuf.write(Long.valueOf(3));
		testBuf.write(Long.valueOf(4));
		testBuf.write(Long.valueOf(5));

		
		assert(testBuf._sum == 15);
	}
	
	@Test
	void testCircBufferWrappedTotal() {
		CircularBufferOfLongs testBuf = new CircularBufferOfLongs(5);
		testBuf.write(Long.valueOf(1));
		testBuf.write(Long.valueOf(2));
		testBuf.write(Long.valueOf(3));
		testBuf.write(Long.valueOf(4));
		testBuf.write(Long.valueOf(5));
		testBuf.write(Long.valueOf(6));

		assert(testBuf._sum == 20);
	}
	


	@Test
	void simpleExerciseTest() {
		String[] program = {
				"35",
				"20",
				"15",
				"25",
				"47",
				"40",
				"62",
				"55",
				"65",
				"95",
				"102",
				"117",
				"150",
				"182",
				"127",
				"219",
				"299",
				"277",
				"309",
				"576"};
		
		Day9Encoder encoder = new Day9Encoder(5);
		
		ArrayList<Long> lines = new ArrayList<>();
		for (String readLine : program) {
			if (readLine.length() > 0) {
				Long readNumber = Long.parseLong(readLine.trim());
				lines.add(readNumber);

				if (!encoder.loadLine(readNumber)) {
					break;
				}
			}
		} 
		int lineNum = lines.size()-1;
		long brokenValue = lines.get(lineNum);
		assert(brokenValue==127);
	}
	
	
	
	@Test
	void partBGetSequenceAddingTo_simpleSeq() {
		String[] program = {
				"35",
				"20",
				"15",
				"25",
				"47",
				"40",
				"62",
				"55",
				"65",
				"95",
				"102",
				"117",
				"150",
				"182",
				"127",
				"219",
				"299",
				"277",
				"309",
				"576"};
		
		Day9Encoder encoder = new Day9Encoder(2);
		
		
		
		ArrayList<Long> lines = new ArrayList<>();
		for (String readLine : program) {
			if (readLine.length() > 0) {
				Long readNumber = Long.parseLong(readLine.trim());
				lines.add(readNumber);
				encoder.loadLine(readNumber);
				if (encoder._buffer._sum == 120) {
					break;
				}
			}
		} 
		assert(encoder._buffer._sum == 120);
	}
	
	@Test
	void partBGetSequenceLengthAddingTo() {
		String[] program = {
				"35",
				"20",
				"15",
				"25",
				"47",
				"40",
				"62",
				"55",
				"65",
				"95",
				"102",
				"117",
				"150",
				"182",
				"127",
				"219",
				"299",
				"277",
				"309",
				"576"};
		
		Day9Encoder encoder = null; 
		
		int sequenceLength = 2;
		long searchSum = 262;
		boolean finished = false;
		
		while (!finished) {
			encoder = new Day9Encoder(sequenceLength);
			for (String readLine : program) {
				if (readLine.length() > 0) {
					Long readNumber = Long.parseLong(readLine.trim());
					encoder.loadLine(readNumber);
					if (encoder._buffer._sum == searchSum) {
						finished = true;
						break;
					}
				}
			}
			if (!finished) {
				logger.debug("Not found at length "+sequenceLength);
				sequenceLength++;
			}
		}
		logger.info("Found at length "+sequenceLength);
		
		assert(sequenceLength == 3);
	}
	
	@Test
	void partBFindWeakness() {
		String[] program = {
				"35",
				"20",
				"15",
				"25",
				"47",
				"40",
				"62",
				"55",
				"65",
				"95",
				"102",
				"117",
				"150",
				"182",
				"127",
				"219",
				"299",
				"277",
				"309",
				"576"};
		
		Day9Encoder encoder = null; 
		
		int sequenceLength = 2;
		long searchSum = 127;
		boolean finished = false;
		
		while (!finished) {
			encoder = new Day9Encoder(sequenceLength);
			for (String readLine : program) {
				if (readLine.length() > 0) {
					Long readNumber = Long.parseLong(readLine.trim());
					encoder.loadLine(readNumber);
					if (encoder._buffer._sum == searchSum) {
						finished = true;
						break;
					}
				}
			}
			if (!finished) {
				logger.debug("Not found at length "+sequenceLength);
				sequenceLength++;
			}
		}
		logger.info("Found at length "+sequenceLength);
		
		assert(sequenceLength == 4);
		assert(encoder._buffer.getSumOfSmallestAndBiggest()==62);
	}	
	
	void getSumTest() {
		String[] program = {
				"35",
				"20",
				"15",
				"25",
				"47",
				"40"
};
		
		CircularBufferOfLongs buf = new CircularBufferOfLongs(6);
		

		for (String readLine : program) {
			if (readLine.length() > 0) {
				Long readNumber = Long.parseLong(readLine.trim());
				buf.write(readNumber);
			}
		}
		
		assert(buf.getSumOfSmallestAndBiggest()==62);
	}	
	
}
