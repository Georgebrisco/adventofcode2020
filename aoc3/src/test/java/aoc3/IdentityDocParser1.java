package aoc3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IdentityDocParser1 {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testInitializeFromSimpleString() {
		IdentityDocument id = new IdentityDocument();
		String def = "eyr:2028 iyr:2016 byr:1995 ecl:oth pid:543685203 hcl:#c0946f hgt:152cm cid:252";
		id.initializeFromString(def);;
		assert(id._pid.equals("543685203"));
	}
	@Test
	void testInitializeFromBrokenString() {
		IdentityDocument id = new IdentityDocument();
		String def = "eyr:2028 iyr:2016 byr:1995 ecl:oth pid:543685203\n hcl:#c0946f hgt:152cm cid:252";
		id.initializeFromString(def);;
		assert(id._pid.equals("543685203"));
	}

	@Test
	void testMissingPassportId() {
		IdentityDocument id = new IdentityDocument();
		String def = "eyr:2028 iyr:2016 byr:1995 ecl:oth hcl:#c0946f hgt:152cm cid:252";
		id.initializeFromString(def);;
		assert(id.isPassport()==false);
	}

	@Test
	void testGoodPassportId() {
		IdentityDocument id = new IdentityDocument();
		String def = "eyr:2028 pid:Apple iyr:2016 byr:1995 ecl:oth hcl:#c0946f hgt:152cm cid:252";
		id.initializeFromString(def);;
		assert(id.isPassport()==true);
	}
	
	@Test
	void testBadFromFile()  {
		IdentityDocument id = new IdentityDocument();
		String def = "hcl:#733820 hgt:155cm\r\n"
				+ "iyr:2013 byr:1989 pid:728471979\r\n"
				+ "ecl:grn eyr:2022";
		id.initializeFromString(def);;
		assert(id.isPassport()==true);
	}
	
	@Test
	void testCaseFromFile()  {
		String defs = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\r\n"
				+ "byr:1937 iyr:2017 cid:147 hgt:183cm\r\n"
				+ "\r\n"
				+ "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\r\n"
				+ "hcl:#cfa07d byr:1929\r\n"
				+ "\r\n"
				+ "hcl:#ae17e1 iyr:2013\r\n"
				+ "eyr:2024\r\n"
				+ "ecl:brn pid:760753108 byr:1931\r\n"
				+ "hgt:179cm\r\n"
				+ "\r\n"
				+ "hcl:#cfa07d eyr:2025 pid:166559648\r\n"
				+ "iyr:2011 ecl:brn hgt:59in";

		String lines[] = defs.split("\r\n");
		String identification = "";
		int totalCount =0;
		int validCount = 0;
		
		for (String readLine : lines) {
        	if (readLine.length()==0) {
        		IdentityDocument doc = new IdentityDocument();
        		doc.initializeFromString(identification.trim());
        		identification = "";
        		totalCount++;
        		if (doc.isPassport()) {
        			validCount++;
        		}
        	} else {
        		identification += " "+readLine;
        	}
			
		}
		
		
		assert(validCount==2);
	}

	@Test
	void testValidateYearBadDate()  {
		assert(IdentityDocument.isYearValid("199", 1968, 2020)==false);
	}
	
	@Test
	void testValidateYearDateAsString()  {
		assert(IdentityDocument.isYearValid("Nineteen Sixty Eight", 1968, 2020)==false);
	}
	@Test
	void testValidateYearMuchTooEarly()  {
		assert(IdentityDocument.isYearValid("1919", 1968, 2020)==false);
	}
	@Test
	void testValidateYearABitTooEarly()  {
		assert(IdentityDocument.isYearValid("1967", 1968, 2020)==false);
	}
	@Test
	void testValidateYearFIrstValid()  {
		assert(IdentityDocument.isYearValid("1968", 1968, 2020)==true);
	}
	@Test
	void testHeightGeneralFormat() {
		assert(IdentityDocument.isHeightFormatValid("22in"));
		assert(IdentityDocument.isHeightFormatValid("2in"));
		assert(IdentityDocument.isHeightFormatValid("232cm"));
		assert(IdentityDocument.isHeightFormatValid("2cm"));

		assert(IdentityDocument.isHeightFormatValid("in")==false);
		assert(IdentityDocument.isHeightFormatValid("2sjs1181in")==false);
		assert(IdentityDocument.isHeightFormatValid("incm")==false);
		assert(IdentityDocument.isHeightFormatValid("")==false);
		assert(IdentityDocument.isHeightFormatValid("122")==false);
	}

	@Test
	void testHeightActualValues() {
		String heightStr="59in";
		assert(IdentityDocument.isHeightValid("59in"));
		assert(IdentityDocument.isHeightValid("76in"));
		
		assert(IdentityDocument.isHeightValid("150cm"));
		assert(IdentityDocument.isHeightValid("193cm"));

		assert(IdentityDocument.isHeightValid("58in")==false);
		assert(IdentityDocument.isHeightValid("77in")==false);
		assert(IdentityDocument.isHeightValid("149cm")==false);
		assert(IdentityDocument.isHeightValid("194cm")==false);
	}
	

	@Test
	void testHairColours() {

		assert(IdentityDocument.isHairColourValid("#ffffff"));
		assert(IdentityDocument.isHairColourValid("#000000"));
		assert(IdentityDocument.isHairColourValid("#829272"));
		assert(IdentityDocument.isHairColourValid("#123456"));
		assert(IdentityDocument.isHairColourValid("#812839"));
		assert(IdentityDocument.isHairColourValid("#383839"));
		assert(IdentityDocument.isHairColourValid("#383839"));
		assert(!IdentityDocument.isHairColourValid("383839"));
		assert(!IdentityDocument.isHairColourValid("#FFFFFF"));
		assert(!IdentityDocument.isHairColourValid("#33839"));
		assert(!IdentityDocument.isHairColourValid("#32283839"));
		assert(!IdentityDocument.isHairColourValid("Green"));


	}
	
	@Test
	void testEyeColours() {
		// amb blu brn gry grn hzl oth
		assert(IdentityDocument.isEyeColourValid("amb"));
		assert(IdentityDocument.isEyeColourValid("blu"));
		assert(IdentityDocument.isEyeColourValid("brn"));
		assert(IdentityDocument.isEyeColourValid("gry"));
		assert(IdentityDocument.isEyeColourValid("grn"));
		assert(IdentityDocument.isEyeColourValid("hzl"));
		assert(IdentityDocument.isEyeColourValid("oth"));

		
		assert(!IdentityDocument.isEyeColourValid("blue"));
		assert(!IdentityDocument.isEyeColourValid("br"));
		assert(!IdentityDocument.isEyeColourValid("gdy"));
		assert(!IdentityDocument.isEyeColourValid("green"));
		assert(!IdentityDocument.isEyeColourValid("h2"));


	}

	
	@Test
	void testPassportNumbers() {
		// amb blu brn gry grn hzl oth
		assert(IdentityDocument.isPassportNumberValid("123456789"));
		assert(IdentityDocument.isPassportNumberValid("000012345"));
		assert(IdentityDocument.isPassportNumberValid("929292929"));
		assert(IdentityDocument.isPassportNumberValid("121212121"));
		assert(IdentityDocument.isPassportNumberValid("239371191"));

		
		assert(!IdentityDocument.isPassportNumberValid("123"));
		assert(!IdentityDocument.isPassportNumberValid("br"));
		assert(!IdentityDocument.isPassportNumberValid("111 122 111"));
	}

	
	
}
