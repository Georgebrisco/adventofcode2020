package aoc3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SeatDecodingTests {

	@Test
	void testSeatLocationDecoderStringSeparation() {
		SeatLocationDecoder decoder = new SeatLocationDecoder("FBFBBFFRLR");
		assert(decoder._rowString.equals("FBFBBFF"));
		assert(decoder._seatString.equals("RLR"));		
	}

	@Test
	void testSeatLocationDecoderRowDecode() {
		SeatLocationDecoder decoder = new SeatLocationDecoder("FBFBBFFRLR");
		assert(decoder._row == 44);
		assert(decoder._seat == 5);
	}

	@Test
	void testSeatIDDecodes() {
//		BFFFBBFRRR: row 70, column 7, seat ID 567.
//		FFFBBBFRRR: row 14, column 7, seat ID 119.
//		BBFFBBFRLL: row 102, column 4, seat ID 820.	
		SeatLocationDecoder decoder = new SeatLocationDecoder("BFFFBBFRRR");
		assert(decoder._row == 70);
		assert(decoder._seat == 7);
		assert(decoder._seatID == 567);
		
		decoder = new SeatLocationDecoder("FFFBBBFRRR");
		assert(decoder._row == 14);
		assert(decoder._seat == 7);
		assert(decoder._seatID == 119);
		
		decoder = new SeatLocationDecoder("BBFFBBFRLL");
		assert(decoder._row == 102);
		assert(decoder._seat == 4);
		assert(decoder._seatID == 820);
		
	}
	@Test
	void testAdditionBetweenTwoNumbers() {
		assert(SeatLocationDecoder.getSumBetween(0,10)==55);
	}
	
}
